package rdev.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rdev.app.model.entity.RoleEntity;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

    Optional<RoleEntity> findByName(String name);

}
