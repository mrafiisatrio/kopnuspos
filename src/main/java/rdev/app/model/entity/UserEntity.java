package rdev.app.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import rdev.app.model.Auditable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
@Table(name = "user", schema = "public", catalog = "Simple_CRUD_App", uniqueConstraints = {@UniqueConstraint(columnNames = {"username"})})
public class UserEntity extends Auditable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "last_login")
    private Date lastLogin;

    @Column(name = "status_login")
    private Boolean statusLogin;

    @Column(name = "is_active")
    private Boolean isActive;

    @PrePersist
    private void onCreate() {
        this.setIsActive(true);
        this.setStatusLogin(true);
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private List<RoleEntity> roleEntityList = new ArrayList<>();

}
