package rdev.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
public class SimpleCrudAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleCrudAppApplication.class, args);
    }

}
