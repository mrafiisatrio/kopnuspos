package rdev.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import rdev.app.dto.user.UserRegisterDTO;
import rdev.app.model.entity.UserEntity;
import rdev.app.exception.UniqueConstraintException;
import rdev.app.exception.UserNotFoundException;
import rdev.app.repository.UserRepository;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserEntity saveUser(UserRegisterDTO userRegisterDTO) throws UniqueConstraintException {
        try {
            UserEntity userEntity = UserEntity.build(0L, userRegisterDTO.getName(), userRegisterDTO.getUsername(), userRegisterDTO.getPassword(), userRegisterDTO.getEmail(), userRegisterDTO.getLastLogin(), userRegisterDTO.getStatusLogin(), userRegisterDTO.getIsActive(), userRegisterDTO.getRoleEntityList());
            return userRepository.save(userEntity);
        } catch (DataIntegrityViolationException e) {
            throw new UniqueConstraintException("Username " + userRegisterDTO.getUsername() + " already Exist.");
        }
    }

    public Iterable<UserEntity> getAllUser() {
        return userRepository.findAll();
    }

    public Optional<UserEntity> getUser(Long id) {
        return userRepository.findById(id);
    }

    public UserEntity getUser(String username, String password) throws UserNotFoundException {
        return userRepository.findByUsernameAndPassword(username, password).orElseThrow(() -> new UserNotFoundException("Username or Password invalid."));
    }

    public UserEntity getUser(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Username not Found."));
    }
}
