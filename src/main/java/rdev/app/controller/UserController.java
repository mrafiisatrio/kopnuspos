package rdev.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rdev.app.dto.user.UserRegisterDTO;
import rdev.app.model.entity.UserEntity;
import rdev.app.exception.UniqueConstraintException;
import rdev.app.service.UserService;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("get-all-user")
    public ResponseEntity<Iterable<UserEntity>> findAllUser() {
        return ResponseEntity.ok(userService.getAllUser());
    }

    @GetMapping("get-user/{id}")
    public ResponseEntity<Optional<UserEntity>> findUser(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getUser(id));
    }

    @PostMapping("register")
    public ResponseEntity<UserEntity> saveUser(@Valid @RequestBody UserRegisterDTO requestBody) throws UniqueConstraintException {
        return new ResponseEntity<UserEntity>(userService.saveUser(requestBody), HttpStatus.CREATED);
    }

}
