package rdev.app.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import rdev.app.model.entity.RoleEntity;
import rdev.app.model.entity.UserEntity;
import rdev.app.service.UserService;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userExisting = userService.getUser(username);
        return new User(userExisting.getUsername(), userExisting.getPassword(), mapRolesToAuthorities(userExisting.getRoleEntityList()));
    }

    private Collection<GrantedAuthority> mapRolesToAuthorities(List<RoleEntity> roleEntityList) {
        return roleEntityList.stream().map(roleEntity -> new SimpleGrantedAuthority(roleEntity.getName())).collect(Collectors.toList());
    }
}
